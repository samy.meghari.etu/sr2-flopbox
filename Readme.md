# SR2_FLOPBOX
##### _MEGHARI Samy_
### Introduction
Le but de se projet est de mettre en place une plateforme **FlopBox** suivant une architechture **REST**. Cette Plateforme nous permettra la gestions de fichiers distants stockés dans des serveurs FTP
### Etat du projet
Toutes les consignes indiquées ont été respectées
### Compilation
Se positionner dans le répertoire SR2-FlopBox.
Compiler avec la commande `mvn clean compile`
Lancer le serveur avec la commande `mvn exec:java`

### Execution
Un fois la compilation effectué, il est possible de commencer à effectuer nos requêtes.
Pour ce faire il vaut mieux utiliser postman qui facile l'executions des requêtes.

### Les requêtes:
Voici ci dessous la liste des requêtes et comment les exécuter sur postman:

    Pour les méthodes newaccount et authentication suivantes il faut mettre dans le body l'identifiant et le password
`newaccount` :  http://127.0.0.1:8080/flopbox/newaccount
 `authentication` : http://127.0.0.1:8080/flopbox/authentication
 
    Une fois l'authentification faite il faut récuperer le token et le passé dans les méthodes restantes.
    les valeurs passées entre [] sont des exemples , elles sont à modifier au gré des utilisateurs
 `add` : http://127.0.0.1:8080/flopbox/server/add/?alias=[ubuntu]&uri=[ubuntu.com]

 `changealias` : http://127.0.0.1:8080/flopbox/server/changealias/?currentAlias=[ubuntu]&newAlias=[myubuntu]

 `changeuri` : http://127.0.0.1:8080/flopbox/server/changeuri/?alias=[ubuntu]&newuri=[myubuntu]

 `deletealias` : http://127.0.0.1:8080/flopbox/server/deletealias/?alias=myubuntu

 `listing` : http://127.0.0.1:8080/flopbox/clientftp/ubuntu/list/pasv/[/cdimage]

 
    Pour les requetes suivantes il faut rentrer l'id et le password dans le header afin de se connecter au serveur ftps
    
`renamerep ` : http://127.0.0.1:8080/flopbox/modifclient/renamerep?oldname=[Documents/mytest/]&newname=[Documents/mytest2/]

`renamefile` : http://127.0.0.1:8080/flopbox/modifclient/renamefile?oldname=[Documents/file.txt]&newname=[Documents/file2.txt]

`mkDir` : http://127.0.0.1:8080/flopbox/modifclient/mkDir?dirpath=Documents/[test2/nouveaudossier]

`deletefile` : http://127.0.0.1:8080/flopbox/modifclient/deletefile?pathfile=[Documents/file2.txt]

`deletedirectory` : http://127.0.0.1:8080/flopbox/modifclient/deletedirectory?parentDir=[Documents/mytest2]&currentDir=[]

`downloadFile` : http://127.0.0.1:8080/flopbox/modifclient/download?path=[/flopbox/downloadfile.txt]

`uploadSingleFile` : http://127.0.0.1:8090/flopbox/modifclient/up?path=[/flopbox/uploadfile.txt]


### Architecture

#### Gestion d'erreur
La gestion des erreurs se fait à l'aide de l'objet **Response**

```
if (!(Main.servers.get(alias) == null)) {
			return Response.status(Response.Status.FORBIDDEN).entity("the server already exist").build();
		}
```

## Code Samples
### Verification de la présence d'un alias avant d'effectuer une requête
```
if (!Main.servers.containsKey(alias)) {
	return Response.status(Response.Status.FORBIDDEN).entity(alias + " is not a alias in the server \n").build();}
```
### Une récupération du chemin à l'aide d'un regex
```
	@DELETE
	@Secured
	@Path("deletealias")
```

### Des méthodes sécurisées :
```
    @NameBinding
    @Retention(RUNTIME)
    @Target({ TYPE, METHOD })
    public @interface Secured {}
```


