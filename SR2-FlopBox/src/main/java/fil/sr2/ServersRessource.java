package fil.sr2;

import java.security.Principal;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * @author Samy MEGHARI
 *
 */
@Path("server")
public class ServersRessource {

	/**
	 * Ajoute un serveur
	 * 
	 * @param alias nom du serveur ajouté
	 * @param uri   lien du serveur ajouté
	 * @return renvoie si l'opération a été faite
	 * @throws NullPointerException
	 */
	@POST
	@Secured
	@Path("add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addServer(@QueryParam("alias") String alias, @QueryParam("uri") String uri)
			throws NullPointerException {
		if (!(Main.servers.get(alias) == null)) {
			return Response.status(Response.Status.FORBIDDEN).entity("the server already exist").build();
		}

		Main.servers.put(alias, uri);
		System.out.println(Main.servers);
		return Response.status(Response.Status.OK).entity("new server added\n").type(MediaType.APPLICATION_JSON)
				.build();
	}

	/**
	 * change un alias
	 * 
	 * @param currentAlias l'alias à modifier
	 * @param newAlias le nouvel alias
	 * @return renvoie si l'opération a été faite
	 */
	@PUT
	@Secured
	@Path("changealias")
	@Produces(MediaType.APPLICATION_JSON)
	public Response changeAlias(@QueryParam("currentAlias") String currentAlias,
			@QueryParam("newAlias") String newAlias) {
		if (!Main.servers.containsKey(currentAlias)) {
			return Response.status(Response.Status.FORBIDDEN).entity(currentAlias + " is not a alias in the server \n")
					.build();
		}
		Main.servers.put(newAlias, Main.servers.remove(currentAlias));
		return Response.status(Response.Status.OK).entity("The new alias " + newAlias + " has been correctly added \n")
				.build();

	}

	/**
	 * change un uri
	 * 
	 * @param alias  l'alias associé à l'uri
	 * @param newuri la nouvelle uri 
	 * @return renvoie si l'opération a été faite
	 */
	@PUT
	@Secured
	@Path("changeuri")
	@Produces(MediaType.APPLICATION_JSON)
	public Response changeUri(@QueryParam("alias") String alias, @QueryParam("newuri") String newuri) {
		if (!Main.servers.containsKey(alias)) {
			return Response.status(Response.Status.FORBIDDEN).entity(alias + " is not a alias in the server \n")
					.build();
		}
		Main.servers.put(alias, newuri);
		return Response.status(Response.Status.OK).entity("The new uri " + newuri + " has been correctly added \n")
				.build();
	}

	/**
	 * supprime un alias
	 * 
	 * @param alias l'alias à supprimer
	 * @return renvoie si l'opération a été faite
	 */
	@DELETE
	@Secured
	@Path("deletealias")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteServer(@QueryParam("alias") String alias) {
		System.out.println(Main.servers);
		System.out.println(alias);

		if (Main.servers.get(alias) == null) {
			return Response.status(Response.Status.BAD_REQUEST).entity("this server doesnt exist").build();
		}
		Main.servers.remove(alias);
		return Response.status(Response.Status.OK).entity("the server " + alias + " has been correctly deleted \n")
				.build();

	}

}
