package fil.sr2;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Samy MEGHARI
 *
 */
@Path("/authentication")
public class Authentication {
	
	/**
	 * Envoie le token au serveur
	 * 
	 * @param username le nom d'utilisateur 
	 * @param password le motdepasse
	 * @return le réponse du serveur
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response authenticateUser(@FormParam("username") String username, @FormParam("password") String password) {

		try {

			authenticate(username, password);

			String token = issueToken(username);

			return Response.ok(token).build();

		} catch (Exception e) {
			return Response.status(Response.Status.FORBIDDEN).build();
		}
	}

	/**
	 *Vérifie les logs
	 * 
	 * @param username le nom d'utilisateur
	 * @param password le mot de passe
	 * @throws Exception
	 */
	private void authenticate(String username, String password) throws Exception {
		Credentials credential = new Credentials(username, password);
		if (!(Main.credentials.contains(credential)))
			throw new Exception("you have input an invalid username");
		if (!(Main.credentials.get(Main.credentials.indexOf(credential)).getPswd().equals(password)))
			throw new Exception("you have input an invalid password");

	}

	/**
	 * Génere un token
	 * 
	 * @param username le nom d'utilisateur 
	 * @return un token
	 */
	private String issueToken(String username) {
		Random random = new SecureRandom();
		String token = new BigInteger(130, random).toString(32);
		Main.tokens.put(username, token);
		System.out.println(Main.tokens);
		return token;

	}

}