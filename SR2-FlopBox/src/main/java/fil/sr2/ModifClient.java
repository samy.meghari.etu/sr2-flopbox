package fil.sr2;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPSClient;
import org.glassfish.jersey.media.multipart.FormDataParam;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.SocketException;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("modifclient")
public class ModifClient {
	private static FTPSClient client = new FTPSClient("SSL");

	/**
	 * @param id  identifiant de connexion
	 * @param mdp mot de passe
	 * @throws SocketException
	 * @throws IOException
	 */
	private static void connect(String id, String mdp) throws SocketException, IOException {
		client.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
		client.connect("ftps.fil.univ-lille1.fr", 21);
		client.login(id, mdp);
		client.sendCommand("PBSZ 0");
		client.sendCommand("PROT P");
		client.setFileType(FTP.BINARY_FILE_TYPE);
		client.enterLocalPassiveMode();

	}

	/**
	 * renome un repertoire
	 * 
	 * @param oldname nom actuel
	 * @param newname nouveau nom
	 * @param id      identifiant de connexion
	 * @param mdp     mot de passe
	 * @return retourne une reponse du serveur
	 * @throws IOException
	 */
	@PUT
	@Secured
	@Path("renamerep")
	@Produces(MediaType.APPLICATION_JSON)
	public static Response RenameRepository(@QueryParam("oldname") String oldname,
			@QueryParam("newname") String newname, @HeaderParam("id") String id, @HeaderParam("mdp") String mdp)
			throws IOException {
		connect(id, mdp);
		client.rename(oldname, newname);

		client.disconnect();
		return Response.status(Response.Status.OK).entity(oldname + "is now " + newname).build();

	}

	/**
	 * renome un file
	 * 
	 * @param oldname nom actuel
	 * @param newname nouveau nom
	 * @param id      identifiant de connexion
	 * @param mdp     mot de passe
	 * @return retourne une reponse du serveur
	 * @throws IOException
	 */
	@PUT
	@Secured
	@Path("renamefile")
	@Produces(MediaType.APPLICATION_JSON)
	public static Response RenameFile(@QueryParam("oldname") String oldname, @QueryParam("newname") String newname,
			@HeaderParam("id") String id, @HeaderParam("mdp") String mdp) throws IOException {
		connect(id, mdp);

		client.rename(oldname, newname);

		client.disconnect();

		return Response.status(Response.Status.OK).entity(oldname + " is now " + newname).build();

	}

	/**
	 * supprime un file
	 * 
	 * @param filename le nom du file
	 * @param id       identifiant de connexion
	 * @param mdp      mot de passe
	 * @return retourne une reponse du serveur
	 * @throws IOException
	 */
	@DELETE
	@Secured
	@Path("deletefile")
	@Produces(MediaType.APPLICATION_JSON)
	public static Response deletemyFile(@QueryParam("pathfile") String filename, @HeaderParam("id") String id,
			@HeaderParam("mdp") String mdp) throws IOException {
		connect(id, mdp);

		client.deleteFile(filename);

		client.disconnect();

		return Response.status(Response.Status.OK).entity(filename + " has been correctly deleted").build();

	}

	/**
	 * supprime un repertoire
	 * 
	 * @param parentDir
	 * @param currentDir
	 * @param id         identifiant de connexion
	 * @param mdp        mot de passe
	 * @return supprime un dossier
	 * @throws IOException
	 */
	@DELETE
	@Secured
	@Path("deletedirectory")
	@Produces(MediaType.APPLICATION_JSON)
	public static Response removemyDirectory(@QueryParam("parentDir") String parentDir,
			@QueryParam("currentDir") String currentDir, @HeaderParam("id") String id, @HeaderParam("mdp") String mdp)
			throws IOException {
		connect(id, mdp);

		String dirToList = parentDir;
		if (!currentDir.equals("")) {
			dirToList += "/" + currentDir;
		}

		FTPFile[] subFiles = client.listFiles(dirToList);
		if (subFiles != null && subFiles.length > 0) {

			for (FTPFile aFile : subFiles) {
				String currentFileName = aFile.getName();
				if (currentFileName.equals(".") || currentFileName.equals("..")) {
					continue;
				}
				String filePath = parentDir + "/" + currentDir + "/" + currentFileName;
				if (currentDir.equals("")) {
					filePath = parentDir + "/" + currentFileName;
				}

				if (aFile.isDirectory()) {
					removemyDirectory(dirToList, currentFileName, id, mdp);
				} else {
					boolean deleted = client.deleteFile(filePath);
					if (deleted) {

						System.out.println("DELETED the file: " + filePath);

					} else {
						return Response.status(Response.Status.FORBIDDEN)
								.entity("CANNOT delete the file: " + filePath + " \n").build();

					}
				}
			}

			boolean removed = client.removeDirectory(dirToList);
			if (removed) {
				return Response.status(Response.Status.OK).entity(dirToList + "has been correctly deleted").build();
			} else {

				return Response.status(Response.Status.OK).entity(dirToList + "CANNOT remove the directory").build();

			}
		}

		client.disconnect();

		return Response.status(Response.Status.OK).entity(dirToList + "has been correctly deleted").build();

	}


	/**
	 * crée un dossier
	 * 
	 * @param dirpath le chemin du repertoire
	 * @param id      identifiant de connexion
	 * @param mdp     mot de passe
	 * @return reponse du serveur
	 * @throws IOException
	 */

	@POST
	@Secured
	@Path("mkDir")
	@Produces(MediaType.APPLICATION_JSON)
	public static Response mkDir(@QueryParam("dirpath") String dirpath, @HeaderParam("id") String id,
			@HeaderParam("mdp") String mdp) throws IOException {
		connect(id, mdp);

		String dirToCreate = dirpath;
		boolean success = client.makeDirectory(dirToCreate);
		if (success) {

			client.disconnect();

			return Response.status(Response.Status.OK).entity("Successfully created directory: " + dirToCreate).build();

		} else {
			client.disconnect();

			return Response.status(Response.Status.FORBIDDEN).entity("Failed to create directory. See server's reply.")
					.build();

		}

	}

	/**
	 * upload un fichier
	 * 
	 * @param pathFile          chemin de destination
	 * @param username          identifiant de connexion
	 * @param password          mot de passe
	 * @param uploadInputStream fichier a upload
	 * @return reponse du seveur
	 */
	@POST
	@Path("up")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(@QueryParam("path") String pathFile, @HeaderParam("username") String username,
			@HeaderParam("password") String password, @FormDataParam("file") InputStream uploadInputStream) {

		Response reponse;
		try {

			connect(username, password);
			client.setFileType(FTPClient.BINARY_FILE_TYPE);

			uploadFile2(pathFile, uploadInputStream);
			reponse = Response.status(Response.Status.OK).entity("").build();
		} catch (IOException e) {
			reponse = Response.status(Response.Status.FORBIDDEN).entity("Erreur connection au serveur FTP refusé.\\n")
					.build();
		}
		return reponse;
	}


	public boolean uploadFile2(String pathFile, InputStream uploadInputStream) {
		try {
			client.enterLocalPassiveMode();
			client.setAutodetectUTF8(true);
			boolean success = client.storeFile(pathFile, uploadInputStream);
			uploadInputStream.close();
			return success;
		} catch (IOException e) {
			return false;

		}

	}
	/**
	 * telecharge un fichier 
	 * 
	 * @param pathFile chemin du fichier
	 * @param username identifiant
	 * @param password mot de passe
	 * @return message du seveur
	 */
	@GET
	@Path("download")
	@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.TEXT_PLAIN })
	public Response downloadFile(@QueryParam("path") String pathFile,
			@HeaderParam("username") String username, @HeaderParam("password") String password) {
		Response reponse;

		try {

			connect(username, password);
			client.setFileType(FTPClient.BINARY_FILE_TYPE);


			downloadFile(pathFile);
			File file = new File("/tmp/" + pathFile);
			if (file.exists()) {
				Object test = (Object) file;
				System.out.println(test);
				reponse = Response.status(Response.Status.OK).entity(test).build();
			} else {
				reponse = Response.status(Response.Status.NOT_FOUND).entity("").build();

			}
		} catch (IOException e) {
			reponse = Response.status(Response.Status.FORBIDDEN).entity("Erreur connection au serveur FTP refusé.\\n").build();

		}
		return reponse;
	}
	
	public void downloadFile(String pathFile) {
		try {
			File file = new File("/home/ubuntu/Documents/flopboxlocal/" + pathFile);
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			OutputStream downloaded = new FileOutputStream(file, false);
			client.enterLocalPassiveMode();
			client.setAutodetectUTF8(true);
			String filename = changeToFileDirectory(pathFile);
			boolean success = client.retrieveFile(filename, downloaded);
			if (!success) {
				file.delete();
			}
			downloaded.close();
		} catch (IOException e) {
			System.out.println("Error listing the directory");
		}
	}
	
	private String changeToFileDirectory(String pathFile) throws IOException {
		String[] splitted = pathFile.split("/");
		for (int i = 0; i < splitted.length - 1; i++) {
			client.changeWorkingDirectory(splitted[i]);
		}
		String filename = splitted[splitted.length - 1];
		return filename;
	}




}
