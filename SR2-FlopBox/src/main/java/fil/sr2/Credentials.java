package fil.sr2;

/**
 * @author MEGHRI Samy
 *
 */
public class Credentials {
	private String username;
	private String pswd;

	

	/**
	 * Crée un utilisateur
	 * 
	 * @param username le nom d'utilisateur
	 * @param pswd le mot de passe
	 */
	public Credentials(String username, String pswd) {
		this.username = username;
		this.pswd = pswd;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPswd() {
		return pswd;
	}

	public void setPassword(String pswd) {
		this.pswd = pswd;
	}

	@Override
	public String toString() {
		return getUsername();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Credentials)) {
			return false;
		}
		Credentials u = (Credentials) obj;

		return u.getUsername().trim().equals(getUsername().trim());
	}

}
