package fil.sr2;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

@Path("clientftp")
public class Listing {
	private Credentials user = new Credentials("anonymous", "anonymous");
	private FTPClient clientftp;

	/**
	 * Liste l'arborescence du path
	 * 
	 * @param alias l'alias choisi
	 * @param path  le chemin que l'on veut parcourir
	 * @param mode  pasv our port
	 * @return l'arborescence
	 */
	@GET
	@Secured
	@Path("{alias}/list/{mode}/{path: .*}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response listing(@PathParam("alias") String alias, @PathParam("path") String path,
			@PathParam("mode") String mode) {

		String server = Main.servers.get(alias);
		server = "ftp.ubuntu.com";
		clientftp = new FTPClient();
		clientftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));

		try {
			clientftp.connect(server, 21);
		}

		catch (IOException e) {
			return Response.status(Response.Status.FORBIDDEN)
					.entity("the connection to the server" + server + "is impossible ").build();
		}

		if (mode.equals("pasv")) {
			clientftp.enterLocalPassiveMode();
		}

		else if (!mode.equals("port")) {
			return Response.status(Response.Status.FORBIDDEN)
					.entity(mode + " wrong input,please try with <pasv> or <port>").build();
		}

		try {
			clientftp.login(user.getUsername(), user.getPswd());
		}

		catch (IOException e) {
			return Response.status(Response.Status.UNAUTHORIZED).entity("the connection to the server has failed")
					.build();
		}

		FTPFile[] files;

		try {
			files = clientftp.listFiles(path);
		}

		catch (IOException e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("please try later ").build();
		}

		return Response.status(Response.Status.OK).entity(fileInfos(files)).type(MediaType.APPLICATION_JSON).build();
	}

	/**
	 * Formate les informations des fichiers et dossiers
	 * 
	 * @param subFiles des fichiers
	 * @return les fichiers et dossiers et leurs infos
	 */
	private static String fileInfos(FTPFile[] subFiles) {
		DateFormat mydate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String res = "";

		for (FTPFile afile : subFiles) {
			String infos = afile.getName();

			if (afile.isDirectory()) {
				infos = " / " + infos;
			}
			infos += "		Size:			" + afile.getSize() + "Date:	"
					+ mydate.format(afile.getTimestamp().getTime());
			res += infos + "\n";
		}
		return res;
	}
}
