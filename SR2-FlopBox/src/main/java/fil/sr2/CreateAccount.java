package fil.sr2;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Samy MEGHARI
 *
 */
@Path("newaccount")
public class CreateAccount {

	/**
	 * crée un compte 
	 * 
	 * @param username le nom d'utilisateur
	 * @param password le mot de passe
	 * @return la réponse du serveur
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response createAccount(@FormParam("username") String username, @FormParam("password") String password) {
		Credentials newUser = new Credentials(username, password);
		System.out.println(Main.credentials);

		if (Main.credentials.contains(newUser)) {
			return Response.status(Response.Status.FORBIDDEN).entity("this user already exist").build();
		}

		Main.credentials.add(newUser);
		System.out.println(Main.servers.size());
		String message = "{\"username\": \"" + newUser.getUsername() + "\"}";
		return Response.status(Response.Status.OK).entity(message).type(MediaType.APPLICATION_JSON).build();
	}
}
